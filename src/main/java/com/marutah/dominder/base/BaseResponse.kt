package com.marutah.dominder.base

class BaseResponse<T>(val code: Int = 200,
                      val message: String = "Success") {

    var data: T? = null
}