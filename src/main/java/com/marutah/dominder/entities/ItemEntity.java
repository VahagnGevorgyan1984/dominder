package com.marutah.dominder.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class ItemEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "position", nullable = false)
  private int position;

  @Column(name = "content", nullable = false)
  private String content;

  @Column(name = "isChecked", nullable = false)
  private boolean isChecked;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "noteId", nullable = false)
  private NoteEntity note;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }

  public NoteEntity getNote() {
    return note;
  }

  public void setNote(NoteEntity note) {
    this.note = note;
  }
}
