package com.marutah.dominder.service;

import com.marutah.dominder.dto.note.NoteDto;
import com.marutah.dominder.models.NoteUpdateModel;

public interface NoteService {

  NoteDto addNote(NoteDto noteCreateDto);

  NoteUpdateModel update(NoteUpdateModel noteUpdateModel);

  void delete(Long id);
}
