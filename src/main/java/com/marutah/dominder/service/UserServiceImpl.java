package com.marutah.dominder.service;

import com.marutah.dominder.dto.UserEditDto;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {


  @Autowired
  private UserRepository userRepository;

  @Override
  public UserEntity findByUserName(String userName) {

    return userRepository.findByUsername(userName).orElseThrow(RuntimeException::new);
  }

  @Override
  @Transactional
  public UserEditDto update(UserEditDto editDto) {
    UserEntity userEntity = userRepository.findById(editDto.getId()).orElseThrow(RuntimeException::new);

    if (editDto.getName() != null) {
      userEntity.setName(editDto.getName());
    }
    if (editDto.getFcmToken() != null) {
      userEntity.setFcmToken(editDto.getFcmToken());
    }
    if (editDto.getImage() != null) {
      userEntity.setImage(editDto.getImage());
    }
    if (editDto.getSurname() != null) {
      userEntity.setSurname(editDto.getSurname());
    }
    if (editDto.getEmail() != null) {
      userEntity.setEmail(editDto.getEmail());
    }

    userRepository.save(userEntity);
    return editDto;
  }

  @Override
  @Transactional(readOnly = true)
  public List<UserEditDto> getByIds(List<Long> userIds) {
    List<UserEntity> users = userRepository.findAllById(userIds);
    List<UserEditDto> userEditDtos = new ArrayList<>();
    for (UserEntity entity : users) {
      UserEditDto userEditDto = new UserEditDto();
      userEditDto.setId(entity.getId());
      userEditDto.setUsername(entity.getUsername());
      userEditDto.setEmail(entity.getEmail());
      userEditDto.setName(entity.getName());
      userEditDto.setSurname(entity.getSurname());
      userEditDto.setImage(entity.getImage());
      userEditDto.setFcmToken(entity.getFcmToken());
      userEditDtos.add(userEditDto);
    }
    return userEditDtos;
  }
}
