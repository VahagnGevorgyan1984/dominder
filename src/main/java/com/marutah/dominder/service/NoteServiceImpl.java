package com.marutah.dominder.service;

import com.marutah.dominder.dto.item.ItemDto;
import com.marutah.dominder.dto.note.NoteDto;
import com.marutah.dominder.entities.ItemEntity;
import com.marutah.dominder.entities.NoteEntity;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.models.ItemCreateModel;
import com.marutah.dominder.models.NoteUpdateModel;
import com.marutah.dominder.repository.NoteRepository;
import com.marutah.dominder.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NoteServiceImpl implements NoteService {

  @Autowired
  NoteRepository noteRepository;

  @Autowired
  UserRepository userRepository;

  @Override
  @Transactional
  public NoteDto addNote(NoteDto noteCreateDto) {

    UserEntity userEntity = userRepository.findById(noteCreateDto.getUserId()).orElseThrow(RuntimeException::new);

    NoteEntity noteEntity = new NoteEntity();
    noteEntity.setGroupId(noteCreateDto.getGroupId());
    noteEntity.setAlarmTime(noteCreateDto.getAlarmTime());
    noteEntity.setPlaceName(noteCreateDto.getPlaceName());
    noteEntity.setPlaceAddress(noteCreateDto.getPlaceAddress());
    noteEntity.setLongitude(noteCreateDto.getLongitude());
    noteEntity.setLatitude(noteCreateDto.getLatitude());
    noteEntity.setMessage(noteCreateDto.getMessage());
    noteEntity.setOwner(userEntity);

    List<ItemEntity> itemEntityList = new ArrayList<>();
    for (ItemDto noteDto : noteCreateDto.getItems()) {
      ItemEntity itemEntity = new ItemEntity();
      itemEntity.setContent(noteDto.getContent());
      itemEntity.setPosition(noteDto.getPosition());
      itemEntity.setChecked(noteDto.isChecked());
      itemEntity.setNote(noteEntity);
      itemEntityList.add(itemEntity);
    }

    noteEntity.setItems(itemEntityList);
    noteRepository.save(noteEntity);
    return noteCreateDto;
  }

  @Override
  @Transactional
  public NoteUpdateModel update(NoteUpdateModel noteUpdateModel) {
    NoteEntity entity = noteRepository.findById(noteUpdateModel.getId()).orElseThrow(RuntimeException::new);
    if (noteUpdateModel.getMessage() != null) {
      entity.setMessage(noteUpdateModel.getMessage());
    }
    if (noteUpdateModel.getAlarmTime() != 0) {
      entity.setAlarmTime(noteUpdateModel.getAlarmTime());
    }
    if (noteUpdateModel.getPlaceName() != null) {
      entity.setPlaceName(noteUpdateModel.getPlaceName());
    }
    if (noteUpdateModel.getPlaceAddress() != null) {
      entity.setPlaceAddress(noteUpdateModel.getPlaceAddress());
    }
    if (noteUpdateModel.getLongitude() != 0) {
      entity.setLongitude(noteUpdateModel.getLongitude());
    }
    if (noteUpdateModel.getLatitude() != 0) {
      entity.setLatitude(noteUpdateModel.getLatitude());
    }
    if (noteUpdateModel.getGroupId() != null) {
      entity.setGroupId(noteUpdateModel.getGroupId());
    }
    if (noteUpdateModel.getItems() != null) {
      List<ItemEntity> itemEntityList = new ArrayList<>();
      for (ItemCreateModel noteDto : noteUpdateModel.getItems()) {
        ItemEntity itemEntity = new ItemEntity();
        itemEntity.setContent(noteDto.getContent());
        itemEntity.setPosition(noteDto.getPosition());
        itemEntity.setChecked(noteDto.isChecked());
        itemEntity.setNote(entity);
        itemEntityList.add(itemEntity);
      }

      entity.setItems(itemEntityList);
    }
    noteRepository.save(entity);
    return noteUpdateModel;
  }

  @Override
  @Transactional
  public void delete(Long id) {
    noteRepository.deleteById(id);
  }

}
