package com.marutah.dominder.service;

import com.marutah.dominder.dto.group.GroupDto;
import java.util.List;

public interface GroupService {

  GroupDto addGroup(GroupDto groupCreateDto);

  GroupDto updateGroup(GroupDto groupCreateDto);

  void delete(Long id);

  List<GroupDto> getAll();
}
