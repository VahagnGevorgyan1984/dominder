package com.marutah.dominder.service;

import com.marutah.dominder.dto.UserEditDto;
import com.marutah.dominder.entities.UserEntity;
import java.util.List;

public interface UserService {

  UserEntity findByUserName(String userName);

  UserEditDto update(UserEditDto editDto);

  public List<UserEditDto> getByIds(List<Long> userIds);
}
