package com.marutah.dominder.service;

import com.marutah.dominder.HeaderRequestInterceptor;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AndroidPushNotificationsService {

  private static final String FIREBASE_SERVER_KEY = "AAAACH7acjg:APA91bG0bL_x_20yr6oAW6FqkdS1rGfVQOtFy3SFJANESma1PlrDLqcUFEUz-ZVu5qlhqJUd0q1izktW7XKS2tyqGF2nxbjrKaIsKY5iCLDF5Rbt9RfElyyh2eUfLw4XiaB5RfctJhPO";
  private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

  @Autowired
  private UserRepository userRepository;

  public String sendPushForGroupCreation(String groupName, String ownerName, Set<Long> userIds) {
    //List<String> tokens = userRepository.findAllById(userIds).stream().map(UserEntity::getFcmToken).collect(Collectors.toList());

    if(userIds==null || userIds.isEmpty()) return null;

    List<UserEntity> userEntities = userRepository.findAllById(userIds);

    ArrayList<String> fcmTokens = new ArrayList<>();
    for (UserEntity userEntity : userEntities) {
      String fsmToken = userEntity.getFcmToken();
      if (fsmToken != null) {
        fcmTokens.add(fsmToken);
      }
    }
    if (fcmTokens.isEmpty()) return  null;

    return sendPush(groupName, ownerName + " invited group " + groupName, fcmTokens);
  }

  public String sendPushForNoteUpdate(String noteName, String changerName, Set<Long> userIds) {
    //List<String> tokens = userRepository.findAllById(userIds).stream().map(UserEntity::getFcmToken).collect(Collectors.toList());

    if(userIds==null || userIds.isEmpty()) return null;

    List<UserEntity> userEntities = userRepository.findAllById(userIds);

    ArrayList<String> fcmTokens = new ArrayList<>();
    for (UserEntity userEntity : userEntities) {
      String fsmToken = userEntity.getFcmToken();
      if (fsmToken != null) {
        fcmTokens.add(fsmToken);
      }
    }
    if (fcmTokens.isEmpty()) return  null;

    return sendPush(noteName, changerName + " invited group " + noteName, fcmTokens);
  }
  private String sendPush(String title, String bodyMessage, List<String> userTokens) {
    JSONObject body = new JSONObject();
    body.put("to", userTokens);
    body.put("priority", "high");

//    JSONObject notification = new JSONObject();
//    notification.put("title", title);
//    notification.put("body", bodyMessage);

    JSONObject data = new JSONObject();
    data.put("Key-1", "JSA Data 1");
    data.put("Key-2", "JSA Data 2");

//    body.put("notification", notification);
    body.put("data", data);
    HttpEntity<String> request = new HttpEntity<>(body.toString());

    CompletableFuture<String> pushNotification = send(request);
    CompletableFuture.allOf(pushNotification).join();

    try {
      return pushNotification.get();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Async
  protected CompletableFuture<String> send(HttpEntity<String> entity) {

    RestTemplate restTemplate = new RestTemplate();

    /**
     https://fcm.googleapis.com/fcm/send
     Content-Type:application/json
     Authorization:key=FIREBASE_SERVER_KEY*/

    ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
    interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
    interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
    restTemplate.setInterceptors(interceptors);

    String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

    return CompletableFuture.completedFuture(firebaseResponse);
  }
}
