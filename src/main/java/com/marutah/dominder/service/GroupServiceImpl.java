package com.marutah.dominder.service;

import com.marutah.dominder.dto.group.GroupDto;
import com.marutah.dominder.entities.GroupEntity;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.repository.GroupRepository;
import com.marutah.dominder.repository.UserRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GroupServiceImpl implements GroupService {

  @Autowired
  private GroupRepository repository;

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public GroupDto addGroup(GroupDto groupCreateDto) {
    List<UserEntity> userEntities = userRepository.findAllById(groupCreateDto.getUsers());
    GroupEntity groupEntity = new GroupEntity();
    groupEntity.setName(groupCreateDto.getName());
    groupEntity.setImage(groupCreateDto.getImage());
    groupEntity.setOwnerId(groupCreateDto.getOwnerId());
    groupEntity.setUsers(new HashSet<>(userEntities));
    repository.save(groupEntity);

    groupCreateDto.setId(groupEntity.getId());
    return groupCreateDto;
  }

  @Override
  @Transactional
  public GroupDto updateGroup(GroupDto groupDto) {
    List<UserEntity> userEntities = userRepository.findAllById(groupDto.getUsers());
    GroupEntity groupEntity = repository.findById(groupDto.getId()).orElseThrow(RuntimeException::new);
    if (groupDto.getName() != null) {
      groupEntity.setName(groupDto.getName());
    }
    if (groupDto.getImage() != null) {
      groupEntity.setImage(groupDto.getImage());
    }
    if (groupDto.getUsers() != null) {
      groupEntity.getUsers().addAll(userEntities);
    }
    repository.save(groupEntity);
    return groupDto;
  }

  @Override
  @Transactional
  public void delete(Long id) {
    repository.deleteById(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<GroupDto> getAll() {
    List<GroupEntity> groupEntities = repository.findAll();

    List<GroupDto> groupDtos = new ArrayList<>();

    for (GroupEntity groupEntity : groupEntities) {
      GroupDto groupDto = new GroupDto();
      groupDto.setId(groupEntity.getId());
      groupDto.setOwnerId(groupEntity.getOwnerId());
      groupDto.setUsers(groupEntity.getUsers().stream().map(UserEntity::getId).collect(Collectors.toSet()));
      groupDto.setName(groupEntity.getName());
      groupDto.setImage(groupEntity.getImage());
      groupDtos.add(groupDto);
    }
    return groupDtos;
  }
}

