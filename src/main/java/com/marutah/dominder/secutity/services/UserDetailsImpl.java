package com.marutah.dominder.secutity.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.marutah.dominder.entities.RoleEntity;
import com.marutah.dominder.entities.UserEntity;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsImpl implements UserDetails {

  private static final long serialVersionUID = 1L;

  private Long id;
  private String image;
  private String username;
  private String email;
  private String name;
  private String surname;
  private Collection<? extends GrantedAuthority> authorities;
  @JsonIgnore
  private String password;

  public UserDetailsImpl(Long id, String image, String username, String email, String name, String surname,
      Collection<? extends GrantedAuthority> authorities, String password) {
    this.id = id;
    this.image = image;
    this.username = username;
    this.email = email;
    this.name = name;
    this.surname = surname;
    this.authorities = authorities;
    this.password = password;
  }

  public static UserDetailsImpl bulid(UserEntity user) {

    Set<RoleEntity> roles = user.getRoles();

    Stream<RoleEntity> rolesStream = roles.stream();

    Stream<SimpleGrantedAuthority> authorityStream = rolesStream.map(role -> {
          String roleName = role.getName().name();
          return new SimpleGrantedAuthority(roleName);
        }
    );
    List<GrantedAuthority> authorities = authorityStream.collect(Collectors.toList());

//    List<GrantedAuthority> authorities = user.getRoles().stream()
//        .map(role -> new SimpleGrantedAuthority(role.getName().name()))
//        .collect(Collectors.toList());

    return new UserDetailsImpl(user.getId(), user.getImage(), user.getUsername(), user.getEmail(), user.getName(), user.getSurname(), authorities,
        user.getPassword());

  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  public Long getId() {
    return id;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getEmail() {
    return email;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDetailsImpl user = (UserDetailsImpl) o;
    return Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return 1;
  }
}
