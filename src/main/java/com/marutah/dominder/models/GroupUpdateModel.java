package com.marutah.dominder.models;

public class GroupUpdateModel extends GroupCreateModel {

  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
