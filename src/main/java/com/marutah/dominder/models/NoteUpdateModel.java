package com.marutah.dominder.models;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;

public class NoteUpdateModel {

  private Long id;
  private double longitude;
  private double latitude;
  private Long groupId;
  @NotNull
  private Long userId;
  @NotNull
  private long alarmTime;
  private String message;
  private String placeAddress;
  private String placeName;
  private List<ItemCreateModel> items = new ArrayList<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public List<ItemCreateModel> getItems() {
    return items;
  }

  public void setItems(List<ItemCreateModel> items) {
    this.items = items;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public long getAlarmTime() {
    return alarmTime;
  }

  public void setAlarmTime(long alarmTime) {
    this.alarmTime = alarmTime;
  }

  public String getPlaceAddress() {
    return placeAddress;
  }

  public void setPlaceAddress(String placeAddress) {
    this.placeAddress = placeAddress;
  }

  public String getPlaceName() {
    return placeName;
  }

  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }
}
