package com.marutah.dominder.models;

import java.util.Set;
import javax.validation.constraints.NotNull;

public class GroupCreateModel {

  @NotNull
  private Set<Long> users;
  @NotNull
  private String name;

  private String image;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Set<Long> getUsers() {
    return users;
  }

  public void setUsers(Set<Long> users) {
    this.users = users;
  }
}
