package com.marutah.dominder.models;

public enum ERole {

  ROLE_USER, ROLE_ADMIN, ROLE_OWNER
}
