package com.marutah.dominder.models

import java.util.*
import javax.validation.constraints.NotEmpty

data class NoteCreateModel(
        var longitude: Double = 0.0,
        var latitude: Double = 0.0,
        var groupId: Long? = null,
        @NotEmpty
        var alarmTime: Long = 0,
        var message: String? = null,
        var placeName: String? = null,
        var placeAddress: String? = null,
        var items: List<ItemCreateModel> = ArrayList()
)