package com.marutah.dominder.controllers;

import com.marutah.dominder.base.BaseResponse;
import com.marutah.dominder.dto.group.GroupDto;
import com.marutah.dominder.entities.GroupEntity;
import com.marutah.dominder.models.GroupCreateModel;
import com.marutah.dominder.models.GroupUpdateModel;
import com.marutah.dominder.repository.GroupRepository;
import com.marutah.dominder.service.AndroidPushNotificationsService;
import com.marutah.dominder.service.GroupService;
import com.marutah.dominder.service.UserService;
import io.swagger.annotations.Api;
import java.lang.reflect.Field;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("GroupController")
@Api(value = "Group", description = "Operations pertaining to products in Online Store")
public class GroupController {

  @Autowired
  UserService userService;
  @Autowired
  GroupRepository groupRepository;
  @Autowired
  private GroupService groupService;
  @Autowired
  private AndroidPushNotificationsService pushNotificationsService;

  @PostMapping("/api/group")
  public ResponseEntity<?> create(@RequestBody @Valid GroupCreateModel groupCreateModel, Principal principal) {
    GroupDto groupCreateDto = new GroupDto();
    Long userId = userService.findByUserName(principal.getName()).getId();
    groupCreateDto.setOwnerId(userId);
    groupCreateModel.getUsers().add(userId);
    groupCreateDto.setUsers(groupCreateModel.getUsers());
    groupCreateDto.setName(groupCreateModel.getName());
    groupCreateDto.setImage(groupCreateModel.getImage());
    pushNotificationsService.sendPushForGroupCreation(groupCreateModel.getName(), principal.getName(), groupCreateModel.getUsers());
    BaseResponse<GroupDto> response = new BaseResponse();
    response.setData(groupService.addGroup(groupCreateDto));
    return ResponseEntity.ok(response);
  }

  @PutMapping("/api/group")
  public ResponseEntity<?> update(@RequestBody @Valid GroupUpdateModel groupUpdateModel) {
    GroupDto groupCreateDto = new GroupDto();
    groupCreateDto.setId(groupUpdateModel.getId());
    groupCreateDto.setUsers(groupUpdateModel.getUsers());
    groupCreateDto.setImage(groupUpdateModel.getImage());
    groupCreateDto.setName(groupUpdateModel.getName());
    BaseResponse<GroupDto> response = new BaseResponse();
    response.setData(groupService.updateGroup(groupCreateDto));
    return ResponseEntity.ok(response);
  }

  @DeleteMapping("/api/group/{id}")
  public void delete(@PathVariable Long id) {
    groupService.delete(id);
  }

  @GetMapping("/api/groups")
  public ResponseEntity<?> getGroups() {
    List<GroupDto> groups = groupService.getAll();
    BaseResponse<List<GroupDto>> response = new BaseResponse();
    response.setData(groups);
    return ResponseEntity.ok(response);
  }

}
