package com.marutah.dominder.controllers;

import com.marutah.dominder.base.BaseResponse;
import com.marutah.dominder.entities.RoleEntity;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.models.ERole;
import com.marutah.dominder.repository.RoleRepository;
import com.marutah.dominder.repository.UserRepository;
import com.marutah.dominder.request.LoginRequest;
import com.marutah.dominder.request.SignupRequest;
import com.marutah.dominder.response.JwtResponse;
import com.marutah.dominder.response.MessageResponse;
import com.marutah.dominder.secutity.jwt.JwtUtils;
import com.marutah.dominder.secutity.services.UserDetailsImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController("AuthController")
@RequestMapping("/api/auth")
@Api(value = "Authentication", description = "Operations pertaining to products in Online Store")
public class AuthController {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserDetailsService userDetailsService;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @ApiOperation(value = "Login")
  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    String jwtRefresh = jwtUtils.createRefreshToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    BaseResponse<JwtResponse> response = new BaseResponse();
    response.setData(new JwtResponse(
        jwt,
        jwtRefresh,
        userDetails.getId(),
        userDetails.getUsername(),
        userDetails.getEmail(),
        userDetails.getName(),
        userDetails.getSurname(),
        userDetails.getImage(),
        roles));

    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "Register")
  @PostMapping("/register")
  @Transactional
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }

    // Create new user's account
    UserEntity user = new UserEntity(
        signUpRequest.getUsername(),
        signUpRequest.getName(),
        signUpRequest.getSurname(),
        signUpRequest.getEmail(),
        encoder.encode(signUpRequest.getPassword()),
        signUpRequest.getImage()
);

    Set<String> strRoles = signUpRequest.getRole();
    Set<RoleEntity> roles = new HashSet<>();

    if (strRoles == null) {
      RoleEntity userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            RoleEntity adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);
            break;
          case "owner":
            RoleEntity ownerRole = roleRepository.findByName(ERole.ROLE_OWNER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(ownerRole);
            break;
          default:
            RoleEntity userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }
    user.setRoles(roles);
    UserEntity writtenEntity = userRepository.save(user);



    UserDetails userDetails = userDetailsService.loadUserByUsername(signUpRequest.getUsername());
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
        userDetails, signUpRequest.getPassword(), userDetails.getAuthorities());

    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    String jwt = jwtUtils.generateJwtToken(usernamePasswordAuthenticationToken);
    String jwtRefresh = jwtUtils.createRefreshToken(usernamePasswordAuthenticationToken);
    BaseResponse<JwtResponse> response = new BaseResponse();
    response.setData(new JwtResponse(jwt,
        jwtRefresh,
        writtenEntity.getId(),
        writtenEntity.getUsername(),
        writtenEntity.getEmail(),
        writtenEntity.getName(),
        writtenEntity.getSurname(),
        writtenEntity.getImage(),
        new ArrayList(strRoles == null ? Arrays.asList(ERole.ROLE_USER) : strRoles)));
    return ResponseEntity.ok(response);
  }
}
