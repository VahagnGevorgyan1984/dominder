package com.marutah.dominder.controllers;

import com.marutah.dominder.base.BaseResponse;
import com.marutah.dominder.dto.UserEditDto;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.models.UserEditModel;
import com.marutah.dominder.models.UsersModel;
import com.marutah.dominder.repository.UserRepository;
import com.marutah.dominder.service.UserService;
import java.security.Principal;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("UserController")
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @PutMapping("/api/user")
  public ResponseEntity<?> update(@RequestBody UserEditModel editModel, Principal principal) {
    Long userId = userService.findByUserName(principal.getName()).getId();
    UserEditDto editDto = new UserEditDto();

    editDto.setId(userId);
    editDto.setFcmToken(editModel.getFcmToken());
    editDto.setImage(editModel.getImage());
    editDto.setName(editModel.getName());
    editDto.setSurname(editModel.getSurname());
    editDto.setEmail(editModel.getEmail());
    BaseResponse<UserEditDto> response = new BaseResponse();
    response.setData(userService.update(editDto));
    return ResponseEntity.ok(response);
  }

  @GetMapping("/api/users")
  public ResponseEntity<?> users() {

    List<UserEntity> users = userRepository.findAll();
    BaseResponse<List<UserEntity>> response = new BaseResponse();
    response.setData(users);
    return ResponseEntity.ok(response);
  }

  @GetMapping("api/search/{text}")
  public ResponseEntity<?> searchUser(@PathVariable String text) {
    List<UserEntity> users = userRepository.findUsersByKeyword(text);
    BaseResponse<List<UserEntity>> response = new BaseResponse();
    response.setData(users);
    return ResponseEntity.ok(response);
  }

  @PostMapping("api/users/ids")
  public ResponseEntity<?> getByIds(@RequestBody UsersModel usersModel) {

    List<UserEditDto> users=userService.getByIds(usersModel.getUserIds());
    BaseResponse<List<UserEditDto>> response = new BaseResponse();
    response.setData(users);
    return ResponseEntity.ok(response);
  }
}
