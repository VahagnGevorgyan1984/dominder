package com.marutah.dominder.controllers;

import com.marutah.dominder.base.BaseResponse;
import com.marutah.dominder.dto.item.ItemDto;
import com.marutah.dominder.dto.note.NoteDto;
import com.marutah.dominder.entities.GroupEntity;
import com.marutah.dominder.entities.ItemEntity;
import com.marutah.dominder.entities.NoteEntity;
import com.marutah.dominder.entities.UserEntity;
import com.marutah.dominder.models.ItemCreateModel;
import com.marutah.dominder.models.NoteCreateModel;
import com.marutah.dominder.models.NoteUpdateModel;
import com.marutah.dominder.repository.GroupRepository;
import com.marutah.dominder.repository.NoteRepository;
import com.marutah.dominder.secutity.services.UserDetailsImpl;
import com.marutah.dominder.service.AndroidPushNotificationsService;
import com.marutah.dominder.service.NoteService;
import com.marutah.dominder.service.UserService;
import io.swagger.annotations.Api;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("NoteController")
@Api(value = "Note", description = "Operations pertaining to products in Online Store")
public class NoteController {

  @Autowired
  UserService userService;

  @Autowired
  NoteService noteService;

  @Autowired
  NoteRepository noteRepository;

  @Autowired
  GroupRepository groupRepository;

  @Autowired
  private AndroidPushNotificationsService pushNotificationsService;

  @PostMapping("/api/note")
  public ResponseEntity<?> createNote(@RequestBody @Valid NoteCreateModel createModel, @AuthenticationPrincipal UserDetailsImpl user) {

    NoteDto createDto = new NoteDto();
    createDto.setUserId(user.getId());
    fromModelToDto(createModel, createDto);
    BaseResponse<NoteDto> response = new BaseResponse();
    response.setData(noteService.addNote(createDto));
    return ResponseEntity.ok(response);
  }

  @PutMapping("api/note")
  public ResponseEntity<?> update(@RequestBody NoteUpdateModel noteUpdateModel, @AuthenticationPrincipal UserDetailsImpl user, Principal principal){
    noteUpdateModel.setUserId(user.getId());
    BaseResponse<NoteUpdateModel> response = new BaseResponse();
    response.setData(noteService.update(noteUpdateModel));

    NoteEntity entity = noteRepository.findById(noteUpdateModel.getId()).orElseThrow(RuntimeException::new);
    if(entity.getGroupId()!=null){
      Long groupId = entity.getGroupId();
      if (groupId != 0) {
        GroupEntity groupEntity = groupRepository.findById(groupId).orElseThrow(RuntimeException::new);
        Set<UserEntity> userEntities = groupEntity.getUsers();
        Set<Long> userIds = userEntities.stream().map(UserEntity::getId).collect(Collectors.toSet());
        Long loginedUserId = userService.findByUserName(principal.getName()).getId();
        for (Iterator<Long> iterator = userIds.iterator(); iterator.hasNext(); ) {
          Long id = iterator.next();
          if (id == loginedUserId) {
            iterator.remove();
          }
        }
        pushNotificationsService.sendPushForNoteUpdate(noteUpdateModel.getMessage(), principal.getName(), userIds);
        return ResponseEntity.ok(response);
      }
    }

    return ResponseEntity.ok(response);
  }

  @DeleteMapping("/api/note")
  public ResponseEntity<?> delete(@RequestBody NoteUpdateModel noteUpdateModel) {

    noteService.delete(noteUpdateModel.getId());
    BaseResponse<NoteUpdateModel> response = new BaseResponse();
    response.setData(noteUpdateModel);
    return ResponseEntity.ok(response);
  }

  @GetMapping("api/notes")
  public ResponseEntity<?> getNoteByUserId(Principal principal) {
    Long userId = userService.findByUserName(principal.getName()).getId();
    List<NoteEntity> noteEntities = noteRepository.findAllByOwner_Id(userId);
    List<NoteDto> noteDtos = new ArrayList<>();
    List<ItemDto> itemDtos = new ArrayList<>();
    for (NoteEntity entity : noteEntities) {
      NoteDto noteDto = new NoteDto();

      noteDto.setGroupId(entity.getGroupId());

      noteDto.setMessage(entity.getMessage());
      getItemsFromEntity(entity.getItems(), itemDtos);
      noteDto.setItems(itemDtos);
      noteDtos.add(noteDto);
    }
    BaseResponse<List<NoteDto>> response = new BaseResponse();
    response.setData(noteDtos);
    return ResponseEntity.ok(response);
  }

  @GetMapping("api/group/{groupId}/notes")
  public ResponseEntity<?> getNotesByGroupId(@PathVariable Long groupId) {

    List<NoteEntity> noteEntities = noteRepository.findAllByGroupId(groupId);
    List<NoteDto> noteDtos = new ArrayList<>();
    List<ItemDto> itemDtos = new ArrayList<>();
    for (NoteEntity entity : noteEntities) {
      NoteDto noteDto = new NoteDto();

      noteDto.setGroupId(entity.getGroupId());
      noteDto.setMessage(entity.getMessage());
      getItemsFromEntity(entity.getItems(), itemDtos);
      noteDto.setItems(itemDtos);
      noteDtos.add(noteDto);
    }
    BaseResponse<List<NoteDto>> response = new BaseResponse();
    response.setData(noteDtos);
    return ResponseEntity.ok(response);
  }

  private void fromModelToDto(NoteCreateModel createModel, NoteDto createDto) {
    createDto.setLongitude(createModel.getLongitude());
    createDto.setLatitude(createModel.getLatitude());
    createDto.setAlarmTime(createModel.getAlarmTime());
    createDto.setPlaceName(createModel.getPlaceName());
    createDto.setPlaceAddress(createModel.getPlaceAddress());
    createDto.setMessage(createModel.getMessage());
    createDto.setGroupId(createModel.getGroupId());

    List<ItemDto> itemCreateDtoList = new ArrayList<>();
    for (ItemCreateModel model : createModel.getItems()) {
      ItemDto itemCreateDto = new ItemDto();
      itemCreateDto.setContent(model.getContent());
      itemCreateDto.setPosition(model.getPosition());
      itemCreateDto.setChecked(model.isChecked());
      itemCreateDtoList.add(itemCreateDto);
    }
    createDto.setItems(itemCreateDtoList);
  }

  private void getItemsFromEntity(List<ItemEntity> itemEntities, List<ItemDto> itemDtos) {
    for (ItemEntity entity : itemEntities) {
      ItemDto itemDto = new ItemDto();
      itemDto.setChecked(entity.isChecked());
      itemDto.setContent(entity.getContent());
      itemDto.setPosition(entity.getPosition());
      itemDtos.add(itemDto);

    }
  }
}
