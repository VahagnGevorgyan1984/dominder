package com.marutah.dominder.controllers;

import com.marutah.dominder.UploadFileResponse;
import com.marutah.dominder.base.BaseResponse;
import com.marutah.dominder.service.FileStorageService;
import com.marutah.dominder.service.UserService;
import java.io.IOException;
import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class FileUploadController {

  private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

  @Autowired
  private FileStorageService fileStorageService;

  @Autowired
  private UserService userService;

  @PostMapping("/api/uploadFile")
  public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, Principal principal) {
    Long userId = userService.findByUserName(principal.getName()).getId();
    String fileName = fileStorageService.storeFile(file, userId.toString());

    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
        .path("/downloadFile/")
        .path(userId.toString()+"/")
        .path(fileName)
        .toUriString();
    BaseResponse<UploadFileResponse> response = new BaseResponse();
    response.setData(new UploadFileResponse(fileName, fileDownloadUri,
        file.getContentType(), file.getSize()));
    return ResponseEntity.ok(response);
  }

  @PostMapping("/api/uploadFile/free")
  public ResponseEntity<?> uploadFileFree(@RequestParam("file") MultipartFile file) {

    String fileName = fileStorageService.storeFile(file,"noauth");

    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
        .path("/downloadFile/")
        .path(fileName)
        .toUriString();
    BaseResponse<UploadFileResponse> response = new BaseResponse();
    response.setData(new UploadFileResponse(fileName, fileDownloadUri,
        file.getContentType(), file.getSize()));
    return ResponseEntity.ok(response);
  }

  @GetMapping("/downloadFile/{id}/{fileName:.+}")
  public ResponseEntity<?> downloadFile(@PathVariable String id, @PathVariable String fileName, HttpServletRequest request) {
    // Load file as Resource
    Resource resource = fileStorageService.loadFileAsResource(fileName, id);

    // Try to determine file's content type
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    } catch (IOException ex) {
      logger.info("Could not determine file type.");
    }

    // Fallback to the default content type if type could not be determined
    if (contentType == null) {
      contentType = "application/octet-stream";
    }

 return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
        .body(resource);
  }

  @GetMapping("/downloadFile/{fileName:.+}")
  public ResponseEntity<?> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
    // Load file as Resource
    Resource resource = fileStorageService.loadFileAsResource(fileName, "noauth");

    // Try to determine file's content type
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    } catch (IOException ex) {
      logger.info("Could not determine file type.");
    }

    // Fallback to the default content type if type could not be determined
    if (contentType == null) {
      contentType = "application/octet-stream";
    }

    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
        .body(resource);
  }
}

