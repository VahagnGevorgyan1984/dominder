package com.marutah.dominder;

import com.marutah.dominder.entities.RoleEntity;
import com.marutah.dominder.models.ERole;
import com.marutah.dominder.repository.RoleRepository;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@SpringBootApplication(exclude = {RepositoryRestMvcAutoConfiguration.class})
public class DominderApplication extends SpringBootServletInitializer {

  private static Class<DominderApplication> applicationClass = DominderApplication.class;

  public static void main(String[] args) {
    SpringApplication.run(applicationClass, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(applicationClass);
  }

  @Bean
  public WebServerFactoryCustomizer<TomcatServletWebServerFactory> containerCustomizer() {
    return (container -> {
      final ErrorPage errorPage = new ErrorPage("/error");
      container.addErrorPages(errorPage);
    });
  }
}

@Component
class SenndImplementor implements ApplicationRunner {

  @Autowired
  private RoleRepository roleRepository;

  @Override
  public void run(ApplicationArguments args) throws Exception {

    List<RoleEntity> roles = roleRepository.findAll();
    if (!roles.isEmpty()) {
      return;
    }
    insertRoles();
  }

  private void insertRoles() {
    RoleEntity roleOwner = new RoleEntity();
    roleOwner.setName(ERole.ROLE_OWNER);
    RoleEntity roleAdmin = new RoleEntity();
    roleAdmin.setName(ERole.ROLE_ADMIN);
    RoleEntity roleUser = new RoleEntity();
    roleUser.setName(ERole.ROLE_USER);

    List<RoleEntity> roles = Arrays.asList(roleOwner, roleAdmin, roleUser);

    roleRepository.saveAll(roles);
  }
}
