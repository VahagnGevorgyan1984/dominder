package com.marutah.dominder.dto.note;

import com.marutah.dominder.dto.item.ItemDto;
import java.util.ArrayList;
import java.util.List;

public class NoteDto {

  private double latitude;
  private double longitude;
  private Long groupId;
  private long alarmTime;
  private String message;
  private String placeAddress;
  private String placeName;
  private Long userId;
  private List<ItemDto> items = new ArrayList<>();


  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public List<ItemDto> getItems() {
    return items;
  }

  public void setItems(List<ItemDto> items) {
    this.items = items;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public long getAlarmTime() {
    return alarmTime;
  }

  public void setAlarmTime(long alarmTime) {
    this.alarmTime = alarmTime;
  }

  public String getPlaceAddress() {
    return placeAddress;
  }

  public void setPlaceAddress(String placeAddress) {
    this.placeAddress = placeAddress;
  }

  public String getPlaceName() {
    return placeName;
  }

  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }
}
