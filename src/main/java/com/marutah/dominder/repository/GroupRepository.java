package com.marutah.dominder.repository;

import com.marutah.dominder.entities.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

//@RepositoryRestResource(path = "group")
public interface GroupRepository extends JpaRepository<GroupEntity, Long> {

}
