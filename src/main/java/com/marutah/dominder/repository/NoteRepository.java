package com.marutah.dominder.repository;

import com.marutah.dominder.entities.NoteEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends JpaRepository<NoteEntity, Long> {


  List<NoteEntity> findAllByOwner_Id(Long ownerId);
  List<NoteEntity> findAllByGroupId(Long groupId);
}
