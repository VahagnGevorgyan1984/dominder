package com.marutah.dominder.repository;

import com.marutah.dominder.entities.RoleEntity;
import com.marutah.dominder.models.ERole;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

  Optional<RoleEntity> findByName(ERole name);

}
