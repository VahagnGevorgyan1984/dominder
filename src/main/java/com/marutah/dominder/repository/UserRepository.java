package com.marutah.dominder.repository;

import com.marutah.dominder.entities.UserEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

//@RepositoryRestResource(path = "user")
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

  // UserEntity findByUserName(String username);

  Optional<UserEntity> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);

  @Query(value="select * from users u where u.name like %:keyword% or u.surname like %:keyword% or u.username like %:keyword%", nativeQuery=true)
  List<UserEntity> findUsersByKeyword(@Param("keyword") String keyword);

}
