package com.marutah.dominder.error;

import java.util.List;

public class ErrorResponse {
  private List<ErrorModel> errorMessage;

  public List<ErrorModel> getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(List<ErrorModel> errorMessage) {
    this.errorMessage = errorMessage;
  }
}
