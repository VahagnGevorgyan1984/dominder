package com.marutah.dominder.error;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = {RestController.class})
public class UncaughtExceptionsControllerAdvice {

  @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<ErrorResponse> handleException(MethodArgumentNotValidException exception) {

    List<ErrorModel> errorMessages = exception.getBindingResult().getFieldErrors().stream()
        .map(err -> new ErrorModel(err.getField(), err.getRejectedValue(), err.getDefaultMessage()))
        .distinct()
        .collect(Collectors.toList());

    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setErrorMessage(errorMessages);
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
}
